# -*- coding: utf-8 -*-

# importing the itasca module to communicate with FLAC and set working
# directory to be the directory containing this file
import itasca as it
it.command('directory input')

# importing modules from the standard python library
import sys
import os

# importing third party modules located in /lib
import pygit2
import numpy as np
import pptx

# # importing the itasca module to communicate with FLAC and set working
# # directory to be the directory containing this file
# it.command('directory input')

import config
import plot




from goldercorerepo.golder.modelling.flac3d import zone
from goldercorerepo.golder.modelling.flac3d import gridpoint
from goldercorerepo.golder.modelling.flac3d import face
from goldercorerepo.golder.modelling.flac3d import model
from goldercorerepo.golder.modelling.flac3d import base

from goldercorerepo.golder.modelling import hoekbrown

from goldercorerepo.golder.modelling import directories
from goldercorerepo.golder.modelling import powerpoint

reload(zone)
reload(gridpoint)
reload(face)
reload(model)
reload(config)
reload(directories)
reload(powerpoint)
reload(base)
reload(plot)
reload(hoekbrown)


class Model(base.BaseModel):
    """ Model object is a container that contains all model related
        subroutines, functions, and parameters.
    """

    def __init__(self, *args, **kwargs):
        super(Model, self).__init__(*args, **kwargs)
        
    def load_config_parameters(self):
        self.material_properties_file = config.MATERIAL_PROPERTIES
        self.prism_locations_file = config.PRISM_LOCATIONS
        self.boundaries = config.MODEL_BOUNDARIES
        self.zone_group_slots = config.ZONE_GROUP_SLOTS
        self.zone_extra_slots = config.ZONE_EXTRA_SLOTS
        self.gridpoint_extra_slots = config.GRIDPOINT_EXTRA_SLOTS
        self.mining_stages = config.MINING_STAGES
        self.constants = config.PHYSICAL_CONSTANTS
        self.fish_reference_stage = config.FISH_REFERENCE_STAGE
        self.fish_stage = config.FISH_STAGE
        self.hb_conversion_groups = config.HB_CONVERSION_GROUPS
        self.hb_slots = config.HB_CONVERSION_SLOTS
        self.creep_ages = config.CREEP_AGES
        self.creep_settings = config.CREEP_SETTINGS

    def set_plotting_paramters(self):

        self.results_plots = [
            plot.VelocityGeology(),
            plot.VelocityIsosurfaceGeology(),
            plot.YieldStates(),
            plot.MaxShearStrainGeology(),
            plot.DisplacementGeology(1),
            plot.DisplacementGeology(2),
            plot.DisplacementGeology(3),
            plot.DisplacementGeology(4),
            plot.DisplacementGeology(5),
            ]

        self.dry_run_plots = [
#            plot.Excavation(),
#            plot.Geology(),
#            plot.ConstitutiveModels(),
#            plot.Friction(),
#            plot.Cohesion(),
#            plot.YoungsModulus(),
#            plot.PoissonsRatio(),
            ]

        self.plot_views = [
            plot.MiddleSection(),
            ]

    def map_geology_groupings(self):
        slot = config.ZONE_GROUP_SLOTS['geology']
        zone.group.apply_mapping(slot, self.geology_mapping)
        
    def get_slot_from_material_name(self, material_name):
        slot_name = self.material_properties[material_name]['slot']
        try:
            return config.ZONE_GROUP_SLOTS[slot_name]
        except KeyError:
            return config.FACE_GROUP_SLOTS[slot_name]
            
    def get_elastic_parameters(self, material_name):
    
        if self.material_properties[material_name]['constitutive_model'] == 'hoek-brown':
            
            if 'young' not in self.material_properties[material_name].keys():

                self.material_properties[material_name]['young'] = hoekbrown.calculate_rm_stiffness(
                    self.material_properties[material_name]['young_intact'],
                    self.material_properties[material_name]['disturbance'],
                    self.material_properties[material_name]['rmr'])

        parameters = {
                'young': self.material_properties[material_name].get('young'),
                'poisson': self.material_properties[material_name].get('poisson'),
                'density': self.material_properties[material_name].get('density'),
            }

        return parameters

    def get_mohr_coulomb_parameters(self, material_name):
  
        material_properties = self.material_properties[material_name]
        
        if 'tension' not in self.material_properties[material_name].keys():
            self.material_properties[material_name]['tension'] = self.material_properties[material_name]['cohesion'] * 0.1
            
        parameters = {
                'friction': self.material_properties[material_name].get('friction'),
                'cohesion': self.material_properties[material_name].get('cohesion'),
                'tension': self.material_properties[material_name].get('tension'),
            }

        if 'brittle' in self.material_properties[material_name].keys():
            brittle = bool(self.material_properties[material_name]['brittle'])
            self.material_properties[material_name]['brittle'] = brittle
            parameters['flag-brittle'] = str(brittle)
            
        return parameters
        
    def get_interface_parameters(self, material_name):
        parameters = {
            'friction': self.material_properties[material_name].get('friction'),
            'cohesion': self.material_properties[material_name].get('cohesion'),
            'stiffness-normal': self.material_properties[material_name].get('normal_stiffness'),
            'stiffness-shear': self.material_properties[material_name].get('shear_stiffness'),
            'tension': self.material_properties[material_name].get('tensile_strength'),
            'bonded-slip': bool(self.material_properties[material_name].get('bonded_slip')),
            'shear-bond-ratio': self.material_properties[material_name].get('shear_bond_ratio'),
            }
        return parameters

    def get_power_law_parameters(self, material_name):
    
        #allow for the fact that some units may be one componenet
        if 'A2' not in self.material_properties[material_name].keys():
             self.material_properties[material_name]['A2'] = self.material_properties[material_name]['A1'] 
        if 'n2' not in self.material_properties[material_name].keys():
             self.material_properties[material_name]['n2'] = self.material_properties[material_name]['n1'] 
        if 'rs1' not in self.material_properties[material_name].keys():
             self.material_properties[material_name]['rs1'] = 10
        if 'rs2' not in self.material_properties[material_name].keys():
             self.material_properties[material_name]['rs2'] = self.material_properties[material_name]['rs1'] 
        
        
        parameters = {
                'constant-1': self.material_properties[material_name].get('A1'),
                'constant-2': self.material_properties[material_name].get('A2'),
                'exponent-1': self.material_properties[material_name].get('n1'),
                'exponent-2': self.material_properties[material_name].get('n2'),
                'stress-reference-1': self.material_properties[material_name].get('rs1'),
                'stress-reference-2': self.material_properties[material_name].get('rs2'),
            }
    
        return parameters
    
    def get_material_parameters(self, material_name, elastic=False, live_only=True, stiffness=False):
        slot = self.get_slot_from_material_name(material_name)
        c_model = self.material_properties[material_name]['constitutive_model']
        parameters = {}
        
        if elastic or c_model == 'elastic':
            c_model = 'elastic'
            parameters = self.get_elastic_parameters(material_name)            
        else:                
            if c_model == 'mohr-coulomb':
                parameters = self.get_mohr_coulomb_parameters(material_name)            
            elif c_model == 'hoek-brown':
                parameters = self.get_hoek_brown_parameters(material_name)
            elif c_model == 'strain-softening':
                parameters = self.get_strain_softening_parameters(material_name)
            elif c_model == 'power':
                parameters = self.get_power_law_parameters(material_name)
            elif c_model == 'interface':
                parameters = self.get_interface_parameters(material_name)
            else:
                if c_model != 'null':
                    print('{} constitutive model does not exist'.format(c_model))
                    raise ValueError
                    
        if stiffness:
            for parameter, value in self.get_elastic_parameters(material_name).items():
                parameters[parameter] = value
            
        parameters = {parameter: value for parameter, value in parameters.items() if value is not None}
        return c_model, parameters


    def set_material_parameters(self, material_name, elastic=False, live_only=True, set_cmodel=True, stiffness=True):
        # this is the c model specified in the input file, c_model below is the flac3d cmodel name
        constitutive_model = self.material_properties[material_name]['constitutive_model']
    
        slot = self.get_slot_from_material_name(material_name)

        c_model, parameters = self.get_material_parameters(material_name, elastic=elastic, stiffness=stiffness)

        if set_cmodel:
            zone.material.set_constitutive_model(material_name, slot, c_model, live_only=live_only)
            zone.group.set_constitutive_model(material_name, slot, constitutive_model, self.zone_group_slots['constitutive_model'])   
        zone.material.set_material_parameters(material_name, slot, parameters, live_only=live_only)


    def set_model_properties(self, slot_name, elastic=False, live_only=True):
        self.set_all_constitutive_models(slot_name, elastic=elastic, live_only=live_only)
        slot = config.ZONE_GROUP_SLOTS[slot_name]
        for material_name, material_properties in (
                self.material_properties.items()):
            if material_properties['slot'] == slot_name:
                print('Assigning {} material properties ...'.format(material_name))
                self.set_material_parameters(
                    material_name, elastic=elastic, live_only=live_only, set_cmodel=False)
                    
    def set_all_constitutive_models(self, slot_name, elastic=False, live_only=True):
        slot = config.ZONE_GROUP_SLOTS[slot_name]
        
        print('Updating constitutive model definitions for slot "{}" ...'.format(slot))
        flac3d_constitutive_models = {}

        for material_name, material_properties in self.material_properties.items():
            if material_properties['slot'] == slot_name:
                c_model, parameters = self.get_material_parameters(material_name, elastic=elastic, live_only=live_only)
                flac3d_constitutive_models[material_name] = c_model

        c_model_array = np.empty_like(it.zonearray.ids(), dtype='S32')
        for material_name, c_model in flac3d_constitutive_models.items():
            in_group = it.zonearray.in_group(material_name, str(slot))
            if live_only:
                in_group = np.logical_and(in_group, it.zonearray.live_mechanical())
            c_model_array[in_group] = c_model
            
            zone.group.set_constitutive_model(
                material_name, slot,
                self.material_properties[material_name]['constitutive_model'],
                self.zone_group_slots['constitutive_model']) 
            
        zone.material.set_constitutive_model_from_array(c_model_array)


    def set_gravity(self):
        model.apply.gravity(
            config.PHYSICAL_CONSTANTS['gravitational-acceleration'])
        model.apply.water_density(config.PHYSICAL_CONSTANTS['water-density'])

    def set_boundary_conditions(self):
        self.boundaries = model.query.model_boundaries()
        fixed_boundaries = self.boundaries
        fixed_boundaries['z'] = (fixed_boundaries['z'][0], None)
        model.condition.set_boundary(self.boundaries)

    def set_initial_stresses(self):
        self.boundaries = model.query.model_boundaries()
        overburden_stress = model.condition.set_overburden_stress(
            self.boundaries['z'][1], self.material_properties['Duperow_Limestone']['density'], config.PHYSICAL_CONSTANTS['gravitational-acceleration'])
        model.condition.set_initial_stresses(ratio=1, overburden=overburden_stress)
        
    def refine_mesh_3d(self):
        #zone1 fine refinement
        #zone2 mid-refinement
        it.command("""
            model range create 'zone0-h' position-x 105 140 position-y 100 200
            model range create 'zone0-v' union group 'LPL_Potash' slot '{slot}' group 'LPL_Roof_Salt' group 'LPL_Floor_Salt' slot '{slot}' group 'UPL_Potash' slot '{slot}' 
            model range create 'zone1-h' position-x 80 165 position-y 80 220
            model range create 'zone1-v' 'zone0-v' 
            model range create 'zone2-h' position-x 0 200 
            model range create 'zone2-v' union 'zone1-v' group 'UPL_Roof_Salt' slot '{slot}' group 'BP_Potash' slot '{slot}' 
            
            ;making zones more cubic
            zone densify segments 4 4 1 range 'zone2-h' position-z -923.70 -925.04
            zone densify segments 2 2 1 range 'zone2-h' group 'LPL_Roof_Salt' slot '{slot}'
            zone densify segments 1 1 2 range 'zone2-h' group 'UPL_Potash' slot '{slot}'
            
            zone densify gradient-limit max-length 15 5 15 repeat range 'zone2-h' 'zone2-v'
            zone densify gradient-limit max-length 15 2 15 repeat range 'zone1-h' 'zone1-v'
            zone densify gradient-limit max-length 15 1 15 repeat range 'zone0-h' 'zone0-v'
        """.format(slot=config.ZONE_GROUP_SLOTS['geology']))
            
        it.command('zone gridpoint merge')
        
    def refine_mesh_2d(self):
        it.command("""
            
          
            model range create 'zone0-h' position-x 105 140 
            model range create 'zone0-v' union group 'LPL_Potash' slot '{slot}' group 'LPL_Roof_Salt' group 'LPL_Floor_Salt' slot '{slot}' group 'UPL_Potash' slot '{slot}' 

            model range create 'zone1-h' position-x 80 165 
            model range create 'zone1-v' 'zone0-v' 
            model range create 'zone2-h' position-x 0 200 
            model range create 'zone2-v' union 'zone1-v' group 'UPL_Roof_Salt' slot '{slot}' group 'BP_Potash' slot '{slot}' 
            
            ;making zones more square
            zone densify segments 4 1 1 range 'zone2-h' position-z -923.70 -925.04
            zone densify segments 4 1 4 range 'zone2-h' position-z -923.70 -919.90
            zone densify segments 4 1 2 range 'zone2-h' group 'LPL_Roof_Salt' slot '{slot}'
            zone densify segments 4 1 8 range 'zone2-h' group 'UPL_Potash' slot '{slot}'
            zone densify segments 4 1 4 range 'zone2-h' group 'LPL_Floor_Salt' slot '{slot}'
            zone densify segments 2 1 2 range 'zone2-h' group 'BP_Potash' slot '{slot}'
            zone densify segments 2 1 2 range 'zone2-h' group 'UPL_Roof_Salt' slot '{slot}'
            
;            zone densify segments 2 1 2 gradient-limit max-length 2 15 15 repeat 1 range 'zone1-h' 'zone1-v'
            zone densify segments 2 1 2 gradient-limit max-length 1 15 15 repeat 1 range 'zone0-h' 'zone0-v'
            
        
        ;further refinement for the very fine model
        ;create 5x5 region
        zone densify segment 1,1,2  range position-x=106.1, 136.8 position-z=-919.9,-919.305
        zone gridpoint initialize position-z -919.7 range position-z=-919.60,-919.61
        zone gridpoint initialize position-z -919.3 range position-z=-919.305
        zone densify segment 16,1,4  range position-x=108, 135 position-z=-919.9,-919.7
        zone densify segment 16,1,8  range position-x=108, 135 position-z=-919.7,-919.3
        
        
        ;above and below
        ;create 10x~10 regions
        zone densify segment 8,1,4 range position-x=108, 135 position-z=-919.9,-920.4
        zone densify segment 8,1,8 range position-x=108, 135 position-z=-919.3,-918.7

       ;create ~20x20 regions
        zone densify segment 4,1,2 range position-x=108, 135 position-z=-920.4, -920.8
        zone densify segment 4,1,4 range position-x=108, 135 position-z=-918.7, -918.1
        
        ;40x40 regions
        zone densify segment 2,1,1 range position-x=108, 135 position-z=-920.8, -921.3
        zone densify segment 2,1,2 range position-x=108, 135 position-z=-918.1, -917.5
        
        ;straight out to the sides
        zone densify segment 8,1,2 range position-x=107, 108 position-z=-919.7,-919.9
        zone densify segment 8,1,4 range position-x=107, 108 position-z=-919.3,-919.7        
        zone densify segment 4,1,1 range position-x=106.1,107 position-z=-919.7,-919.9
        zone densify segment 2,1,1 range position-x=106.1,107 position-z=-919.3,-919.7
        zone densify segment 2,1,2 range position-x=106.5,107 position-z=-919.3,-919.7
        
        zone densify segment 8,1,2 range position-x=135, 135.9 position-z=-919.7,-919.9
        zone densify segment 8,1,4 range position-x=135, 135.9 position-z=-919.3,-919.7        
        zone densify segment 4,1,1 range position-x=136.8,135.9 position-z=-919.7,-919.9
        zone densify segment 2,1,1 range position-x=136.8,135.9 position-z=-919.3,-919.7
        zone densify segment 2,1,2 range position-x=136.3,135.9 position-z=-919.3,-919.7

        ;corners
        ;left
        zone densify segment 4,1,2 range position-x=108, 107 position-z=-919.9,-920.4
        zone densify segment 4,1,4 range position-x=108, 107 position-z=-919.3,-918.7
        zone densify segment 2,1,1 range position-x=108, 107 position-z=-920.4, -920.8
        zone densify segment 2,1,2 range position-x=108, 107 position-z=-918.7, -918.1
        zone densify segment 2,1,1 range position-x=106.1, 107 position-z=-919.9, -920.4
        zone densify segment 2,1,1 range position-x=106.1, 107 position-z=-919.3,-918.7
        zone densify segment 1,1,2 range position-x=106.6, 107 position-z=-919.3,-918.7
        
        ;right
        zone densify segment 4,1,2 range position-x=135, 135.9 position-z=-919.9,-920.4
        zone densify segment 4,1,4 range position-x=135, 135.9 position-z=-919.3,-918.7
        zone densify segment 2,1,1 range position-x=135, 135.9 position-z=-920.4, -920.8
        zone densify segment 2,1,2 range position-x=135, 135.9 position-z=-918.7, -918.1
        zone densify segment 2,1,1 range position-x=136.8, 135.9 position-z=-919.9, -920.4
        zone densify segment 2,1,1 range position-x=136.8, 135.9 position-z=-919.3,-918.7
        zone densify segment 1,1,2 range position-x=136.3, 135.9 position-z=-919.3,-918.7
      
        
        ;floor beam
        ;zone densify segment 2,1,1 range position-x=104,140 position-z=-923.7, -923.95
        zone densify segment 16,1,2  range position-x=108, 135 position-z=-923.7, -923.95
        
        ;above and below
        ;create 10x~10 regions
        zone densify segment 8,1,4 range position-x=108, 135 position-z=-923.7,-923.2
        zone densify segment 8,1,8 range position-x=108, 135 position-z=-923.95,-924.5

       ;create ~20x20 regions
        zone densify segment 4,1,2 range position-x=108, 135 position-z=-923.2, -922.7
        zone densify segment 4,1,4 range position-x=108, 135 position-z=-924.5, -925.1
        
        ;40x40 regions
        zone densify segment 2,1,1 range position-x=108, 135 position-z=-922.7, -922.2
        zone densify segment 2,1,2 range position-x=108, 135 position-z=-925.1, -926.1
        
        ;sides
        zone densify segment 8,1,2 range position-x=107, 108 position-z=-923.7, -923.95 
        zone densify segment 8,1,2 range position-x=135, 135.9 position-z=-923.7, -923.95 
        
        zone densify segment 4,1,2 range position-x=107, 108  position-z=-923.7,-923.2
        zone densify segment 4,1,2 range position-x=135,135.9  position-z=-923.7,-923.2
        
        zone densify segment 2,1,1 range position-x=107, 108  position-z=-923.2, -922.7
        zone densify segment 2,1,1 range position-x=135,135.9  position-z=-923.2, -922.7
        
        zone densify segment 4,1,4 range position-x=107, 108 position-z=-923.95,-924.5
        zone densify segment 4,1,4 range position-x=135,135.9 position-z=-923.95,-924.5
        
        zone densify segment 2,1,2 range position-x=106.1, 107 position-z=-923.95,-924.5
        zone densify segment 2,1,2 range position-x=136.8, 135.9 position-z=-923.95,-924.5
        
        zone densify segment 2,1,2 range position-x=107, 108 position-z=-924.5, -925.1
        zone densify segment 2,1,2 range position-x=135,135.9 position-z=-924.5, -925.1
        
        ;extra to make the interface work
        zone densify segment 2, 1, 2 range position-z=-925,-925.9 position-x=135,136
        zone densify segment 2, 1, 2 range position-z=-925,-925.9 position-x=107,108
        zone densify segment 2, 1, 2 range position-z=-925,-925.5 position-x=108,135
        zone densify segment 2, 1, 2 range position-z=-923.95,-924.1 position-x=107.8,135.2
        zone densify segment 2, 1, 2 range position-z=-919.3,-919.15 position-x=107.8,135.2
         zone densify segment 2, 1, 2 range position-z=-917.5,-916.8 position-x=107.8,135.2

        """.format(slot=config.ZONE_GROUP_SLOTS['geology']))
    
    def additional_groups_tall_rooms():
        it.command("""
            zone group 'ProductionRoom1' slot production_rooms range position-x=7.5,19.5 position-z=-919.9,-918.6
            zone group 'ProductionRoom2' slot production_rooms range position-x=34.5,46.5 position-z=-919.9,-918.6
            zone group 'ProductionRoom3' slot production_rooms range position-x=61.5,73.5 position-z=-919.9,-918.6
            zone group 'ProductionRoom4-Pass1' slot production_rooms range position-x=88.5,94.8 position-z=-919.9,-918.6
            zone group 'ProductionRoom4-Pass2' slot production_rooms range position-x=94.8,100.5 position-z=-919.9,-918.6
            zone group 'ProductionRoom5-Pass1' slot production_rooms range position-x=115.5,121.8 position-z=-919.9,-919.35
            zone group 'ProductionRoom5-Pass21' slot production_rooms range position-x=121.8,127.5 position-z=-919.9,-919.35
        """)
        
    def cut_2d_section(self):
        it.command('zone delete range position-y 0 150')
        it.command('zone delete range position-y 157.5 300')
        it.command('zone densify segments 1 8 1')
        it.command('zone delete range position-y 151 300')
        
    def set_interface_properties(self, sband_depth=False):
        
        clay_seam_properties = self.get_material_parameters('Clay_Seams')[1]
        for clayseam, depth in config.CLAY_SEAMS.items():
            if clayseam == '402':
               # it.command('zone attach delete range position-x=108.1,134.7 position-z {depth}'.format(depth=depth))
                it.command('zone interface "{name}" create by-face separate range position-x=108,135 position-z {depth} '.format(name=clayseam, depth=depth))
            elif clayseam == '406':
                depth1=-920.01
                depth2=-920.02
               # it.command('zone attach delete range position-x=108.1,134.7 position-z {depth1},{depth2}'.format(depth1=depth1, depth2=depth2))
                it.command('zone interface "{name}" create by-face separate range position-x=108.1,134.7 position-z {depth1},{depth2} '.format(name=clayseam, depth1=depth1, depth2=depth2))
            else:
               # it.command('zone attach delete range position-z {depth}'.format(depth=depth))
                it.command('zone interface "{name}" create by-face separate range position-z {depth} '.format(name=clayseam, depth=depth))
            face.material.set_material_parameters(clayseam, config.FACE_GROUP_SLOTS['interfaces'], clay_seam_properties)
            it.command('zone interface "{name}" node initialize-stresses'.format(name=clayseam)) 
        if sband_depth != False:    
            it.command('zone interface "{name}" create by-face separate range position-x=108,135 position-z {sband_depth} '.format(name="shadowband", sband_depth=self.calibration))    
            face.material.set_material_parameters("shadowband", config.FACE_GROUP_SLOTS['interfaces'], clay_seam_properties)
            it.command('zone interface "{name}" node initialize-stresses'.format(name="shadowband")) 
        #to account for the creation of new gridpoints
        self.set_boundary_conditions()
    
    def remove_interfaces(self):
        it.command('zone interface "402" element delete')
        it.command('zone interface "shadowband" element delete')
        it.command('zone attach by-face range position-z -919.29,-919.31')
        it.command('zone attach by-face range position-z -923.94,-923.96')
        

    def generate_room_and_pillar_mesh(self):
    
        number_of_rooms = np.append(np.array(config.NUMBER_OF_ROOMS, dtype=int), 1)
        pillar_dimensions = np.append(np.array(config.PILLAR_DIMENSIONS, dtype=float), 0)
        room_dimensions = np.append(np.array(config.ROOM_DIMENSIONS, dtype=float), 0)
        
        number_of_pillars = number_of_rooms.copy()
        number_of_pillars[0] = number_of_pillars[0] + 1
        
        model_dimensions = (pillar_dimensions + room_dimensions) * number_of_pillars
        
        half_width = (pillar_dimensions + room_dimensions) / 2.0
        
        clay_seam_depths = config.CLAY_SEAMS.values()
        clay_seam_depths.append(config.MINING_FLOOR)
      
        
        for name, strata in config.STRATIGRAPHY.items():
        
            
            unit_clay_seams = []
            for clay_seam_depth in clay_seam_depths:
                if clay_seam_depth < strata['top'] and clay_seam_depth > strata['bottom']:
                    unit_clay_seams.append(clay_seam_depth)
            sub_layers = []

            unit_clay_seams.sort(reverse=True)
            print unit_clay_seams
            print strata

            unit_clay_seams = unit_clay_seams if unit_clay_seams is not None else []

            boundaries = [strata['top']] + unit_clay_seams + [strata['bottom']]
            

            for i in range(len(boundaries) - 1):
                sub_layers.append((boundaries[i], boundaries[i+1]))
                                
            for layer in sub_layers:

                unit_height = layer[0] - layer[1]
                pillar_dimensions[2] = unit_height
                room_dimensions[2] = unit_height
                
                #mesh rooms and pillars
                model_offset = np.array((-half_width[0], 0, layer[1]))
                room_offset = np.array((0.3, 0, 0))
                model.mesh.create_rooms_and_pillars(number_of_pillars, pillar_dimensions, room_dimensions, model_offset, room_offset)
                
                
                #mesh surrounding rock mass
                temp_offset = np.array((-half_width[0] + model_dimensions[0], 0, layer[1]))
                temp_number_of_pillars = number_of_pillars.copy()
                temp_number_of_pillars[0] = temp_number_of_pillars[0] * 2
                temp_room_dimensions = room_dimensions.copy()
                temp_room_dimensions[0] = 0
                model.mesh.create_rooms_and_pillars(temp_number_of_pillars, pillar_dimensions, temp_room_dimensions, temp_offset + room_offset)

                if strata['mining_unit'] is True:
                    zone.group.set_rooms_and_pillars(number_of_pillars, pillar_dimensions, room_dimensions, model_offset, config.ZONE_GROUP_SLOTS['production_rooms'])

                zone.group.set_horizontal_stratigraphy(layer[1], unit_height, name, config.ZONE_GROUP_SLOTS['geology'])

        it.command('zone delete range position-x {minimum} {maximum}'.format(minimum=-half_width[0], maximum=0))
        it.command('zone group "None" slot "{slot}" range position-x {minimum} {maximum}'.format(
            slot=config.ZONE_GROUP_SLOTS['production_rooms'],
            minimum=model_dimensions[0] - room_dimensions[0] - pillar_dimensions[0],
            maximum=model_dimensions[0] * 2))
        zone.group.null_slot('Default')
            
    def group_rooms(self):
        for room in range(config.NUMBER_OF_ROOMS[0]):
            minimum = room * (config.ROOM_DIMENSIONS[0] + config.PILLAR_DIMENSIONS[0]) + config.PILLAR_DIMENSIONS[0] / 2.0
            maximum = minimum + config.ROOM_DIMENSIONS[0]
            it.command('zone group "ProductionRoom{room}-Pass1" slot "{slot}" range position-x {minimum} {maximum} position-z -923.70 -919.90 group "rooms" slot "{slot}"'.format(
                slot=config.ZONE_GROUP_SLOTS['production_rooms'], minimum=minimum, maximum=maximum-5.7, room=room+1))
            it.command('zone group "ProductionRoom{room}-Pass2" slot "{slot}" range position-x {minimum} {maximum} position-z -923.70 -919.90 group "rooms" slot "{slot}"'.format(
                slot=config.ZONE_GROUP_SLOTS['production_rooms'], minimum=maximum-5.7, maximum=maximum, room=room+1))

        zone.group.rename('ProductionRoom1-Pass1', 'ProductionRoom1', config.ZONE_GROUP_SLOTS['production_rooms'])
        zone.group.rename('ProductionRoom1-Pass2', 'ProductionRoom1', config.ZONE_GROUP_SLOTS['production_rooms'])
        zone.group.rename('ProductionRoom2-Pass1', 'ProductionRoom2', config.ZONE_GROUP_SLOTS['production_rooms'])
        zone.group.rename('ProductionRoom2-Pass2', 'ProductionRoom2', config.ZONE_GROUP_SLOTS['production_rooms'])
        zone.group.rename('ProductionRoom3-Pass1', 'ProductionRoom3', config.ZONE_GROUP_SLOTS['production_rooms'])
        zone.group.rename('ProductionRoom3-Pass2', 'ProductionRoom3', config.ZONE_GROUP_SLOTS['production_rooms'])
        
        
        
        stage_divisions = config.STAGE_DIVISIONS
        for i in range(len(stage_divisions) - 1):
            zone.group.set_subset_by_position(
                'ProductionRoom5-Pass1-Stage{stage}'.format(stage=i+1),
                'ProductionRoom5-Pass1',
                config.ZONE_GROUP_SLOTS['production_rooms'],
                stage_divisions[i], stage_divisions[i+1])
            
            zone.group.set_subset_by_position(
                'ProductionRoom5-Pass2-Stage{stage}'.format(stage=len(stage_divisions) - i - 1),
                'ProductionRoom5-Pass2',
                config.ZONE_GROUP_SLOTS['production_rooms'],
                stage_divisions[i], stage_divisions[i+1])

        zone.group.rename('rooms', 'FloorBeam', config.ZONE_GROUP_SLOTS['production_rooms'])
        zone.group.rename('pillars', 'ProductionPillars', config.ZONE_GROUP_SLOTS['production_rooms'])
                
    def group_rooms_2d(self):
        zone.group.rename('ProductionRoom5-Pass1-Stage14', 'ProductionRoom5-Pass1', config.ZONE_GROUP_SLOTS['production_rooms'])
        zone.group.rename('ProductionRoom5-Pass2-Stage13', 'ProductionRoom5-Pass2', config.ZONE_GROUP_SLOTS['production_rooms'])
        
    def group_reaction_force_faces(self):
        it.command("""
            ;bottom
            zone face group "ProductionRoom5-Pass1-Stage1-2D-15m-vert" slot "{slot}" internal range ...
                position-x 115.5 121.8 position-y 150.1 150.9 position-z -917.52
            ;top
            zone face group "ProductionRoom5-Pass1-Stage1-2D-15m-vert" slot "{slot}" internal range ...
                position-x 115.5 121.8 position-y 150.1 150.9 position-z -925.04
            ;left
            zone face group "ProductionRoom5-Pass1-Stage1-2D-15m-horiz" slot "{slot}" internal range ...
                position-x 115.5 position-y 150.1 150.9 position-z -923.7 -919.90
            ;right
            zone face group "ProductionRoom5-Pass1-Stage1-2D-15m-horiz" slot "{slot}" internal range ...
                position-x 121.8 position-y 150.1 150.9 position-z -923.7 -919.90
                
                
            ;bottom
            zone face group "ProductionRoom5-Pass2-Stage1-2D-15m-vert" slot "{slot}" internal range ...
                position-x 121.8 127.5 position-y 150.1 150.9 position-z -917.52
            ;top
            zone face group "ProductionRoom5-Pass2-Stage1-2D-15m-vert" slot "{slot}" internal range ...
                position-x 121.8 127.5 position-y 150.1 150.9 position-z -925.04
            ;right
            zone face group "ProductionRoom5-Pass2-Stage1-2D-15m-horiz" slot "{slot}" internal range ...
                position-x 127.5 position-y 150.1 150.9 position-z -923.7 -919.90
            """.format(slot=config.FACE_GROUP_SLOTS['reaction_forces']))
            
    def regroup_reaction_force_faces(self):
        it.command("""
            ;bottom
            zone face group "ProductionRoom5-Pass1-Stage1-2D-15m-vert" slot "{slot}" internal range ...
                group "ProductionRoom5-Pass1-Stage1-2D-vert"
           
            ;left
            zone face group "ProductionRoom5-Pass1-Stage1-2D-15m-horiz" slot "{slot}" internal range ...
                group "ProductionRoom5-Pass1-Stage1-2D-horiz"
            
                
                
            ;bottom
            zone face group "ProductionRoom5-Pass2-Stage1-2D-15m-vert" slot "{slot}" internal range ...
               group "ProductionRoom5-Pass2-Stage1-2D-vert"

            ;right
            zone face group "ProductionRoom5-Pass2-Stage1-2D-15m-horiz" slot "{slot}" internal range ...
                group "ProductionRoom5-Pass2-Stage1-2D-horiz"
            """.format(slot=config.FACE_GROUP_SLOTS['reaction_forces']))

        
class Run(base.BaseRun):
    def __init__(self):
        excavation_group = config.ZONE_GROUP_SLOTS['excavation']
        surface_group = config.ZONE_GROUP_SLOTS['surface']
        groups_to_overwrite_with_waste = {
            config.ZONE_GROUP_SLOTS['geology']: None,
            config.ZONE_GROUP_SLOTS['fault']: 'None',
            }

        super(Run, self).__init__(excavation_group, add_waste=False)
        
        self.advance_rate = config.ADVANCE_RATE
        self.creep_time_multiplier = config.CREEP_AGE_MULTIPLIER
        
        self.z_displacement_history_x_offsets = [-8.175, -7.2375, -6.3, -4.725, -3.15, -1.575, 0.0, 1.425, 2.85, 4.275, 5.7, 6.6375, 7.575]
        self.z_displacement_history_z_offsets = [-5.3175, -4.27875, -3.24, -2.57, -1.9, 1.9, 2.495, 3.09, 3.685, 4.28, 4.88]
        
        self.x_displacement_history_x_offsets = [-8.175, -7.2375, -6.3, 0, 5.7, 6.6375, 7.575]
        self.x_displacement_history_z_offsets = [-5.3175, -4.27875, -3.24, -2.57, -1.9, -0.95, 0.0, 0.95, 1.9, 2.495, 3.09, 3.685, 4.28, 4.88]

    
    def pore_pressure_adjustments(self):
        pass

    def material_adjustments(self):
        pass
        
    def calibration_adjustments(self, calibration):
        if self.mining_stage_name in ['ProductionRoom5-Pass1-Stage1-2D-15m', 'ProductionRoom5-Pass2-Stage1-2D-15m']:
            if self.dry_run is False:
                model.settings.set_creep_off()
                model.solve.solve_model(
                    solve_ratio=config.CREEP_SETTINGS['solve-ratio-nocreep'],
                    stable=True, interval=5000,
                    material_update=None, velocity_cutoff=None)

    def post_excavation_calibration_adjustments(self, calibration):
        if self.mining_stage_name in ['ProductionRoom5-Pass1-Stage1-2D-15m', 'ProductionRoom5-Pass2-Stage1-2D-15m']:
            horizontal_name=self.mining_stage_name + "-horiz"
            vertical_name=self.mining_stage_name + "-vert"
            self.apply_reaction_forces(vertical_name, horizontal_name)
           
        
    def get_stage_distance(self, stage_name):
        in_group = it.zonearray.in_group(stage_name, config.ZONE_GROUP_SLOTS['production_rooms'])
        gridpoint_positions = it.gridpointarray.pos()
        zone_gridpoints = it.zonearray.gridpoints()[in_group]
        max_y = None
        min_y = None
        for zone_gridpoint_set in zone_gridpoints:
            for gridpoint_index in zone_gridpoint_set:
                gridpoint_y = gridpoint_positions[gridpoint_index][1]
                if max_y is None:
                    max_y = gridpoint_y
                if min_y is None:
                    min_y = gridpoint_y
                
                max_y = max(gridpoint_y, max_y)
                min_y = min(gridpoint_y, min_y)
                
                
        if max_y is not None and min_y is not None:
            distance = max_y - min_y
        else:
            distance = None
                
        return distance
        
    def set_additional_history_points(self):
        model.history.set_history_point('total-creep-time', 'creep time-total', location_type='model')
        model.history.set_history_point('mechanical-timestep', 'timestep', location_type='model')
        model.history.set_history_point('unbalanced-force', 'unbalanced-force', location_type='zone')
                
        reference_position = (121.8, 150, -921.80)
        
        model.history.set_displacement_history_grid(self.z_displacement_history_x_offsets, [0], self.z_displacement_history_z_offsets, reference_position, 'z')
        model.history.set_displacement_history_grid(self.x_displacement_history_x_offsets, [0], self.x_displacement_history_z_offsets, reference_position, 'x')


    def initialize_for_creep(self):
        it.command("model configure creep")
        it.command('model largestrain on')
        it.command('history interval 10')
        
    def apply_reaction_forces(self, vertical_name, horizontal_name):
        current_creep_time = it.zone.creep_time_total()
            
        offset = config.RELAXATION_CURVE_OFFSET*self.creep_time_multiplier
        horiz_half_life=config.RELAXATION_HORIZONTAL_K
        vert_half_life=config.RELAXATION_VERTICAL_K
        
        model.apply.add_logistic_relaxation_table(
            vertical_name, current_creep_time + offset, vert_half_life*self.creep_time_multiplier)
        model.apply.add_logistic_relaxation_table(
            horizontal_name, current_creep_time + offset, horiz_half_life*self.creep_time_multiplier)
        stage_name=self.mining_stage_name
        horiz_face_group = stage_name + "-horiz"
        vert_face_group = stage_name + "-vert"
           
        it.command('zone gridpoint fix velocity 0 0 0 range group "{face_group}" slot "{face_group_slot}"'.format(
            face_group=horiz_face_group, face_group_slot=config.FACE_GROUP_SLOTS['reaction_forces']))
        it.command('zone gridpoint fix velocity 0 0 0 range group "{face_group}" slot "{face_group_slot}"'.format(
            face_group=vert_face_group, face_group_slot=config.FACE_GROUP_SLOTS['reaction_forces']))


        if self.dry_run is False:
            model.settings.set_creep_off()
            model.solve.solve_model(
                solve_ratio=config.CREEP_SETTINGS['solve-ratio-nocreep'],
                stable=True, interval=5000,
                material_update=None, velocity_cutoff=None)

        it.command("""
            zone face apply reaction-x table "{horiz_table_name}" time creep ...
                range group "{horiz_face_group}" slot "{face_group_slot}"
            zone face apply reaction-z table "{horiz_table_name}" time creep ...
                range group "{horiz_face_group}" slot "{face_group_slot}"
            zone face apply reaction-x table "{vert_table_name}" time creep ...
                range group "{vert_face_group}" slot "{face_group_slot}"
            zone face apply reaction-z table "{vert_table_name}" time creep ...
                range group "{vert_face_group}" slot "{face_group_slot}"
            """.format(
                vert_table_name=vertical_name,
                horiz_table_name=horizontal_name,
                vert_face_group=vert_face_group,
                horiz_face_group=horiz_face_group,
                face_group_slot=config.FACE_GROUP_SLOTS['reaction_forces']))

    def stage_cleanup(self, flac3d_model):
        if self.mining_stage_name in ['ProductionRoom5-Pass1-Stage1-2D', 'ProductionRoom5-Pass2-Stage1-2D']:
            it.command('zone face apply-remove reaction-x')
            it.command('zone face apply-remove reaction-z')
        #self.export_creep_histories(self.z_displacement_history_x_offsets, [0.0], self.z_displacement_history_z_offsets, 'z', flac3d_model.output_directories['history_points'], flac3d_model.get_save_file_name())
        #self.export_creep_histories(self.x_displacement_history_x_offsets, [0.0], self.x_displacement_history_z_offsets, 'x', flac3d_model.output_directories['history_points'], flac3d_model.get_save_file_name())

            
    def export_creep_histories(self, flac3d_model, x_offsets, y_offsets, z_offsets, direction, name):
        #if self.mining_stage_name == 'ProductionRoom5-Pass2-Stage1-2D':
            file_path =  flac3d_model.output_directories['history_points']
            save_file_name = flac3d_model.get_save_file_name()
            for x in x_offsets:
                for y in y_offsets:
                    for z in z_offsets:
                        position=((x), (y), (z))
                        displacement_name = '{name}-{direction}-Displacement: ({position[0]}, {position[1]}, {position[2]})'.format(name=name,position=position, direction=direction)
                        case_name = os.path.splitext(os.path.split(save_file_name)[-1])[0]
                        file_dir = os.path.join(file_path, case_name)
                        cal_name = case_name.split('CAL')
                        if len(cal_name) == 1:
                            cal_num = None
                        else:
                            cal_num = float(cal_name[-1][1:])
                        file_name = '{name}{cal_num}-{direction}-({position[0]}_{position[1]}_{position[2]}).csv'.format(name=name, position=position, direction=direction, cal_num=cal_num)
                        if not isinstance(file_dir, unicode):
                            # allows long file names
                            mkdir_file_dir = u'\\\\?\\{path}'.format(path=file_dir)
                        else:
                            mkdir_file_dir = file_dir

                        if not os.path.exists(mkdir_file_dir):
                            os.makedirs(mkdir_file_dir)
                        file_path_name = os.path.join(file_dir, file_name)
                        it.command('history export "{name}" vs "total-creep-time" file "{file_name}" truncate'.format(name=displacement_name, file_name=file_path_name))        

    def resume_creeping(self, flac3d_model, mining_stage_name, additional_creep_time, reset=True):
        creep_time = additional_creep_time
        creep_time *= self.creep_time_multiplier
        print('\tCreep time increment: {0}'.format(creep_time/self.creep_time_multiplier))
        
        model.solve.solve(creep_time=creep_time)
        print('\tTotal creep time: {0}'.format(it.zone.creep_time_total()/self.creep_time_multiplier))

        print('Saving creep model ({0})'.format(mining_stage_name))
        flac3d_model.save(mining_stage_name)
        
    def set_clayseam_histories(self, sbdepth, name):
        sb_z_offsets = [-0.02,-0.04]
        print sb_z_offsets
        sb_x_offsets = [-6.3, -4.725, -3.15, -1.575, 0.0, 1.425, 2.85, 4.275, 5.7]
        print sb_x_offsets
        reference_position = (121.8, 150, sbdepth)
        model.history.set_displacement_history_grid(sb_x_offsets, [0], sb_z_offsets, reference_position, 'z', name)
       
        