import itasca as it
import numpy as np

import config
reload(config)
    

def generate_room_and_pillar_model(number_of_pillars, pillar_dimensions, room_dimensions):
    
    number_of_pillars = np.append(np.array(number_of_pillars, dtype=int), 1)
    pillar_dimensions = np.append(np.array(pillar_dimensions, dtype=float), 0)
    room_dimensions = np.append(np.array(room_dimensions, dtype=float), 0)
    
#    model_dimensions = number_of_pillars * (pillar_dimensions + room_dimensions)
#    rounded_model_dimensions = np.ceil(model_dimensions)
#    
#    model_center = model_dimensions / 2.0
#
#    
    for name, strata in config.STRATIGRAPHY.items():
        room_height = strata['top'] - strata['bottom']

        pillar_dimensions[2] = room_height
        room_dimensions[2] = room_height
        
        offset = np.array((0, 0, strata['bottom']))
        
        mesh_rooms_and_pillars(number_of_pillars, pillar_dimensions, room_dimensions, offset)
        
        if strata['mining_unit'] is True:
            group_rooms_and_pillars(number_of_pillars, pillar_dimensions, room_dimensions, offset, config.ZONE_GROUP_SLOTS['pillars'])

        group_horizontal_stratigraphy(strata['bottom'], room_height, name, config.ZONE_GROUP_SLOTS['geology'])
        
def group_horizontal_stratigraphy(bottom, height, name, pillar_slot):
    top = bottom + height
    it.command("""
        zone group "{name}" slot "{pillar_slot}" range position-z {bottom} {top} 
        """.format(pillar_slot=pillar_slot, bottom=bottom, top=top, name=name))
    
def get_max_element_size(pillar_dimensions):
    minimum_pillar_dimension = np.argmin(pillar_dimensions[:2])
    max_element_size = np.zeros(3)
    
    max_element_size[minimum_pillar_dimension] = pillar_dimensions[minimum_pillar_dimension] / 2
    for i, size in enumerate(pillar_dimensions):
        if i != minimum_pillar_dimension:
            num_elements = size / max_element_size[minimum_pillar_dimension]
            num_elements = num_elements // 2 * 2
            if i == 2:
                num_elements = np.floor(num_elements)
            if num_elements <= 0:
                num_elements = 1.0
                
            max_element_size[i] = size / num_elements
    return max_element_size 
    
def group_rooms_and_pillars(number_of_pillars, pillar_dimensions, room_dimensions, offset, pillar_slot):
    horizontal = np.array((1, 1, 0)) #allows for the extraction of the horizontal components
    vertical = np.array((0, 0, 1)) #allows for the extraction of the vertical components

    pillar_bottom = offset[2]
    pillar_top = pillar_bottom + pillar_dimensions[2]

    it.command("""
        zone group "room" slot "{pillar_slot}" range position-z {bottom} {top} 
        """.format(pillar_slot=pillar_slot, bottom=pillar_bottom, top=pillar_top))

    for x in range(number_of_pillars[0]):
        for y in range(number_of_pillars[1]):
            room_center = (room_dimensions + pillar_dimensions) * np.array((x, y, 0.5)) + offset
            pillar_center = room_center + (horizontal*room_dimensions + horizontal*pillar_dimensions) / 2.0
            
            pillar_min = pillar_center - pillar_dimensions / 2.0
            pillar_max = pillar_center + pillar_dimensions / 2.0

            cmd_str = """
                zone group "pillar" slot "{pillar_slot}" range group "room" slot "{pillar_slot}" ...
                    position-x {pillar_min[0]} {pillar_max[0]} ...
                    position-y {pillar_min[1]} {pillar_max[1]} 
                """.format(pillar_slot=pillar_slot, pillar_min=pillar_min, pillar_max=pillar_max)
            it.command(cmd_str)

    pillar_half_vectors = np.diag(pillar_dimensions)


if __name__ == '__main__':
    it.command('model new')
    generate_room_and_pillar_model(number_of_pillars=(5, 1), pillar_dimensions=(15, 300), room_dimensions=(0, 0))
