from goldercorerepo.golder.modelling.directories import INPUT_DIRECTORY, INPUT_DIRECTORY_LONG
from goldercorerepo.golder.modelling.flac3d import model
reload(model)
import os
import collections
import config

import itasca as it
import numpy as np

# ----------------------------------------------------------------------------
# FISH VARIABLE MAPPING
# ----------------------------------------------------------------------------
FISH_REFERENCE_STAGE = 'PYTHON_REFERENCE_STAGE'
FISH_STAGE = 'PYTHON_STAGE'


# ----------------------------------------------------------------------------
# MODEL PARAMETERS
# ----------------------------------------------------------------------------
MODEL_BOUNDARIES = None

ZONE_GROUP_SLOTS = {
    'refinement': 'refinement',
    'geology': 'geology',
    'fault': 3,
    'excavation': 'excavation',
    'surface': 5,
    'constitutive_model': 'constitutive_model',
    'yielded_zones': 'yielded_zones',
    'production_rooms': 'production_rooms',
    }
    
FACE_GROUP_SLOTS = {
    'interfaces': 'interfaces',
    'reaction_forces': 'reaction_forces',
    }

ZONE_EXTRA_SLOTS = {
    'rmr': 1,
    'intact_ucs': 2,
    'hb_mi': 3,
    'intact_elastic_modulus': 4,
    'poissons_ratio': 5,
    'hb_mb': 6,
    'hb_a': 7,
    'hb_s': 8,
    'calculated_disturbance_factor': 9,
    'disturbance_factor': 10,
    'density': 11,
    'reference_pore_pressure': 12,
    'pit_shell_distance': 13,
    'reference_shear_strain': 14,
    'cumulative_shear_strain': 15,
    }

GRIDPOINT_EXTRA_SLOTS = {
    'reference_displacement': 1,
    'cumulative_displacement': 2,
    }

# ----------------------------------------------------------------------------
# MATERIAL PARAMETERS
# ----------------------------------------------------------------------------
MATERIAL_PROPERTIES = os.path.join(
    INPUT_DIRECTORY,
    '02_Data_Processed',
    '20190508_Old_Material_Properties',
    '2017ShaftPillar_Geo_Inputs-Reformatted.csv')

HB_CONVERSION_GROUPS = {'ubiquitous-joint-hoek-brown': 6}
HB_CONVERSION_SLOTS = [
    ZONE_EXTRA_SLOTS['intact_ucs'],
    ZONE_EXTRA_SLOTS['hb_mb'],
    ZONE_EXTRA_SLOTS['hb_a'],
    ZONE_EXTRA_SLOTS['hb_s'],
]


PHYSICAL_CONSTANTS = {
    'gravitational-acceleration': 9.81,
    'water-density': 1000,
    }

# ----------------------------------------------------------------------------
# CREEP SETTINGS
# ----------------------------------------------------------------------------
CREEP_SETTINGS ={
 'solve-ratio-nocreep' : 5.0e-6,
 'solve-ratio-creep' : 1.0e-99,
 'creep-starting-timestep': 1e-3,
 'creep-lower-bound-ratio': 5.0e-6,
 'creep-upper-bound-ratio': 6.0e-6,
 'creep-latency': 50,
 'creep-low-multiplier': 1.3,
 'creep-up-multiplier': 0.8,
 'creep-max-step' : 1.5e5,
 'creep-min-step': 1e-3,
 }


#multiplier to take creep ages from the units in the dict to seconds, currently set to convert from days
CREEP_AGE_MULTIPLIER = 60*60*24
ADVANCE_RATE = 1552/8.0

#19 days per 1800m room, 8 days cutting plus pass1=1 day delay, pass 2=2 day delay
CREEP_AGES={
'ProductionRoom1': 19 * CREEP_AGE_MULTIPLIER,
'ProductionRoom2': 38 * CREEP_AGE_MULTIPLIER,
'ProductionRoom3': 57 * CREEP_AGE_MULTIPLIER,
'ProductionRoom4-1': 66 * CREEP_AGE_MULTIPLIER,
'ProductionRoom4-2': 76 * CREEP_AGE_MULTIPLIER,
'ProductionRoom5-1a': None,
}

# ----------------------------------------------------------------------------
# MONITORING DATA
# ----------------------------------------------------------------------------
PRISM_LOCATIONS = ''

# ----------------------------------------------------------------------------
# MESHING AND GROUPING
# ----------------------------------------------------------------------------  
NUMBER_OF_ROOMS = (5, 1)
PILLAR_DIMENSIONS = (15, 300)
ROOM_DIMENSIONS = (12, 0)
MINING_FLOOR = -923.7

#NUMBER_OF_ROOMS = (5, 5)
#PILLAR_DIMENSIONS = (30, 15)
#ROOM_DIMENSIONS = (18, 12)

STRATIGRAPHY = {
    'Souris_River_Limestone': {
        'top': -668.54,
        'bottom': -794.04,
        'mining_unit': False,
        },
    'Davidson_Evaporite': {
        'top': -794.04,
        'bottom': -842.14,
        'mining_unit': False,
        },
    'First_Red_Bed': {
        'top': -842.14,
        'bottom': -852.94,
        'mining_unit': False,
        },
    'Hubbard_Salt': {
        'top': -852.94,
        'bottom': -863.13,
        'mining_unit': False,
        },
    'Dawson_Bay_Limestone': {
        'top': -863.13,
        'bottom': -896.43,
        'element_size': 1,
        'mining_unit': False,
        },
    'Second_Red_Bed': {
        'top': -896.43,
        'bottom': -901.34,
        'mining_unit': False,
        },
    'UPL_Roof_Salt': {
        'top': -901.34,
        'bottom': -907.92,
        'mining_unit': False,
        },
    'UPL_Potash': {
        'top': -907.92,
        'bottom': -917.52,
        'mining_unit': False,
        },
    'LPL_Roof_Salt': {
        'top': -917.52,
        'bottom': -919.90,
        'mining_unit': False,
        },
    'LPL_Potash': {
        'top': -919.90,
        'bottom': -925.04,
        'mining_unit': True,
        },
    'LPL_Floor_Salt': {
        'top': -925.04,
        'bottom': -933.35,
        'mining_unit': False,
        },
    'BP_Potash': {
        'top': -933.35,
        'bottom': -943.39,
        'mining_unit': False,
        },
    'BP_Floor_Salt': {
        'top': -943.39,
        'bottom': -1013.63,
        'mining_unit': False,
        },
    }
    
CLAY_SEAMS = {
    '407': -917.52,
#    'Shadow_Band': -919.60,
    '406': -919.90,
    '402': -923.95,
    '401': -925.04,
    }
    
SHADOW_BAND_POSITIONS = [-919.7, -919.60,  -919.50,  -919.40,  -919.3, -919.55, -919.45]


Stage = collections.namedtuple(
    'Stage', ['excavation_group',
              'additional_creep_time',
              ]
            )

STAGE_DIVISIONS = [0, 60, 90, 105, 115, 125, 130, 135, 140, 142.5, 145, 147, 149, 150, 151, 153, 155, 157.5, 160, 165, 170, 175, 185, 195, 210, 240, 300]


excavation_time_beyond_boundary = 626 / ADVANCE_RATE
excavation_time_in_boundary = 300 / ADVANCE_RATE

time_offset_2d = (7.5/8) / ADVANCE_RATE
print ADVANCE_RATE
print time_offset_2d

calibration_offset_2d = 0.5

RELAXATION_CURVE_OFFSET = 0
RELAXATION_HORIZONTAL_K = 0.005
RELAXATION_VERTICAL_K = 0.02

MINING_STAGES = {
    'Elastic-Solution': Stage(
        excavation_group=None,
        additional_creep_time=None),
    'Plastic-Solution': Stage(
        excavation_group=None,
        additional_creep_time=None),
    'ProductionRoom1': Stage(
        excavation_group={'ProductionRoom1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=excavation_time_in_boundary + excavation_time_beyond_boundary*4 + 3),
    'ProductionRoom2': Stage(
        excavation_group={'ProductionRoom2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=excavation_time_in_boundary + excavation_time_beyond_boundary*4 + 3),
    'ProductionRoom3': Stage(
        excavation_group={'ProductionRoom3': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=excavation_time_in_boundary + excavation_time_beyond_boundary*4 + 3),
    'ProductionRoom4-Pass1': Stage(
        excavation_group={'ProductionRoom4-Pass1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=excavation_time_beyond_boundary*2 + 1),
    'ProductionRoom4-Pass2': Stage(
        excavation_group={'ProductionRoom4-Pass2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=excavation_time_beyond_boundary*2 + 2),

    'ProductionRoom1-2D': Stage(
        excavation_group={'ProductionRoom1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=19-time_offset_2d),
    'ProductionRoom2-2D': Stage(
        excavation_group={'ProductionRoom2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=19-time_offset_2d),
    'ProductionRoom3-2D': Stage(
        excavation_group={'ProductionRoom3': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=19-time_offset_2d),
    'ProductionRoom4-Pass1-2D': Stage(
        excavation_group={'ProductionRoom4-Pass1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=9-time_offset_2d),
    'ProductionRoom4-Pass2-2D': Stage(
        excavation_group={'ProductionRoom4-Pass2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=14.04-time_offset_2d),
    'ProductionRoom5-Pass1-Stage1-2D-15m': Stage(
        excavation_group={'ProductionRoom5-Pass1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=0.0258),
    'ProductionRoom5-Pass1-Stage1-2D-20m': Stage(
        excavation_group={'ProductionRoom5-Pass1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=0.0258),
    'ProductionRoom5-Pass1-Stage1-2D-30m': Stage(
        excavation_group={'ProductionRoom5-Pass1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=0.0515),
    'ProductionRoom5-Pass1-Stage1-2D-60m': Stage(
        excavation_group={'ProductionRoom5-Pass1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=0.155),
    'ProductionRoom5-Pass1-Stage1-2D': Stage(
        excavation_group={'ProductionRoom5-Pass1': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=9-time_offset_2d-0.258),
    'ProductionRoom5-Pass2-Stage1-2D-15m': Stage(
        excavation_group={'ProductionRoom5-Pass2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=0.0258),
    'ProductionRoom5-Pass2-Stage1-2D-20m': Stage(
        excavation_group={'ProductionRoom5-Pass2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=0.0258),
    'ProductionRoom5-Pass2-Stage1-2D-30m': Stage(
        excavation_group={'ProductionRoom5-Pass2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=0.0515),
    'ProductionRoom5-Pass2-Stage1-2D-60m': Stage(
        excavation_group={'ProductionRoom5-Pass2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=0.155),
    'ProductionRoom5-Pass2-Stage1-2D': Stage(
        excavation_group={'ProductionRoom5-Pass2': ZONE_GROUP_SLOTS['production_rooms']},
        additional_creep_time=6-time_offset_2d-0.258),

    }
    

additional_creep_times = {
    'ProductionRoom5-Pass1-Stage1': excavation_time_beyond_boundary,
    'ProductionRoom5-Pass1-Stage26': excavation_time_beyond_boundary + 1,
    'ProductionRoom5-Pass2-Stage1': excavation_time_beyond_boundary,
    'ProductionRoom5-Pass2-Stage26': excavation_time_beyond_boundary + 2,
    }

for stage in range(len(STAGE_DIVISIONS) - 1):
    for stage_pass in range(2):
        stage_name = 'ProductionRoom5-Pass{stage_pass}-Stage{stage}'.format(stage=stage+1, stage_pass=stage_pass+1)
        MINING_STAGES[stage_name] = Stage(
            excavation_group={stage_name: ZONE_GROUP_SLOTS['production_rooms']},
            additional_creep_time=additional_creep_times.get(stage_name))