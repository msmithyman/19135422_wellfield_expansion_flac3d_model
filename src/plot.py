
from goldercorerepo.golder.modelling.flac3d import plot
reload(plot)
from goldercorerepo.golder.modelling.flac3d.plot import *

import config
reload(config)

# ----------------------------------------------------------------------------
# Geology Plots
# ----------------------------------------------------------------------------
class Geology(LabelPlot):
    def __init__(self, slot=None, title='Geology', name='Geology',
                 **kwargs):

        if not slot:
            slot = config.ZONE_GROUP_SLOTS['geology']

        color_map = {
            'Waste': (210, 180, 140),
            'Debris': (179, 119, 59),
            'T51_Alluvium': (188, 143, 83),
            'T52_Alluvium': (188, 143, 83),
            'T55_Stebbins_Hill_Tuff': (204, 204, 204),
            'T56_Stebbins_Hill_Epiclastics': (182, 182, 182),
            'T57_Stebbins_Hill_Lake_Sediments': (160, 160, 160),
            'T1_Densely_Welded_Tuff': (217, 219, 181),
            'T2_Poorly_Welded_Tuff': (213, 203, 202),
            'T3_Moderately_Welded_Tuff': (166, 180, 188),
            'T4_Metasedimentary_Rocks': (149, 149, 149),
            'T8_Shoshone_Granite': (200, 200, 200),
            }

        super(Geology, self).__init__(
            slot=slot, color_map=color_map, title=title, name=name, **kwargs)

class Excavation(LabelPlot):
    def __init__(self, slot=None, title='Excavated Material',
                 name='Excavation-Stage', **kwargs):
    
        if not slot:
            slot = config.ZONE_GROUP_SLOTS['excavation']

        # TODO: add alias support
        color_map = {'None': 'black',
                     'Waste': 'darkred',
                     'Excavation': 'darkgreen'}

        filters = [ZoneGroupFilter('None', slot, show=False)]

        super(Excavation, self).__init__(
            slot=slot, color_map=color_map, hide_null='off', title=title,
            name=name, filters=filters, **kwargs)

        self.layers.append(Geology(legend='off'))


class MaxShearStrainGeology(MaxShearStrain):

    def __init__(self, contour_colors=('transparent', 'automatic'),
                 name='Max-Shear-Strain', **kwargs):
                 
        super(MaxShearStrainGeology, self).__init__(
            contour_colors=contour_colors, **kwargs)

        self.layers.append(Geology(legend='off'))


class ConstitutiveModels(LabelPlot):
    def __init__(self, slot=None, title='Constitutive Model',
                 name='Constitutive-Model', **kwargs):

        if not slot:
            slot = config.ZONE_GROUP_SLOTS['constitutive_model']

        color_map = {
            'mohr-coulomb': 'darkred',
            'hoek-brown': 'darkblue',
            'strain-softening': 'pink',
            'elastic': 'darkgreen',
            }

        super(ConstitutiveModels, self).__init__(
            slot=slot, color_map=color_map, title=title, name=name, **kwargs)


class VelocityIsosurfaceGeology(Geology):
    def __init__(self, name='Velocity-IsoSurface', isovalue=1e-04, **kwargs):

        super(VelocityIsosurfaceGeology, self).__init__(name=name, **kwargs)

        self.layers.append(VelocityIsosurface(isovalue=isovalue))
        
        
class VelocityGeology(Velocity):
    def __init__(self, contour_colors=('transparent', 'automatic'),
                 contour_limits=(1e-4, 1e-3, 1e-4), **kwargs):
    
        super(VelocityGeology, self).__init__(contour_colors=contour_colors, contour_limits=contour_limits, **kwargs)
        self.layers.append(Geology(legend='off'))
        
    
class DisplacementGeology(Displacement):
    def __init__(self, contour_max, contour_colors=('transparent', 'automatic'), **kwargs):
        contour_max = float(contour_max)
        contour_min = contour_max/10
        component = 'Magnitude'
        contour_limits = (contour_min, contour_max, contour_min)
        name = 'Displacement-Magnitude-{contour_max}'.format(contour_max=int(contour_max))

        super(DisplacementGeology, self).__init__(
            name=name, contour_limits=contour_limits, component=component,
            contour_colors=contour_colors, **kwargs)
        self.layers.append(Geology(legend='off'))


class YieldStates(LabelPlot):
    def __init__(self, slot=None, title='Yielding Elements',
                 name='Yield-States', **kwargs):

        if not slot:
            slot = config.ZONE_GROUP_SLOTS['yielded_zones']

        color_map = {'None': 'lightgray',
                     'RM Shear': 'red',
                     'RM Tension': 'yellow',
                     'RM Shear & Tension': 'green',
                     'UJ Shear': 'darkorange',
                     'UJ Tension': 'gold',
                     'UJ Shear & Tension': 'lightgreen',
                     'UJ & RM Shear': 'darkviolet',
                     'UJ & RM Tension': 'seagreen',
                     }

        super(YieldStates, self).__init__(
            slot=slot, color_map=color_map, title=title, name=name, **kwargs)

        self.layers.append(VelocityIsosurface())
        self.layers.append(Geology(legend='off'))
        
        
class Stress(ContourPlot):
    def __init__(self, contour_limits=(0, 1e5, 1e4), title=None,
                 name=None, log_scale='off', component='von-mises',
                 **kwargs):

        if not name:
            name = 'Stress-{comp}'.format(comp=component)

        if log_scale == 'on':
            name = '{name}-log'.format(name=name)

        if not title:
            title = 'Stress [{comp}] (psf)'.format(comp=component)

        plot_specifics = 'stress quantity {comp} log {log} '
        plot_specifics = plot_specifics.format(log=log_scale, comp=component)

        super(Stress, self).__init__(
            plot_specifics=plot_specifics,
            contour_limits=contour_limits,
            title=title,
            name=name,
            **kwargs)

class MaxShearStress(Stress):
    def __init__(self, contour_limits=(0, 1e5, 1e4), **kwargs):
        super(MaxShearStress, self).__init__(
            contour_limits=contour_limits,
            component='shear-maximum',
            name='Maximum-Shear',
            **kwargs)
            
class ConfiningStress(Stress):
    def __init__(self, contour_limits=(-30, 10, 5), **kwargs):
        super(ConfiningStress, self).__init__(
            contour_limits=contour_limits,
            contour_colors=('grey', 'black'),
            component='maximum',
            name='Confining-Stress',
            title='Stress [Confining] (MPa)',
            **kwargs)

class Tension(Stress):
    def __init__(self, contour_limits=(0, 20, 2), **kwargs):
        super(Tension, self).__init__(
            contour_limits=contour_limits,
            contour_colors=('grey', 'black'),
            component='maximum',
            name='Tension',
            title='Stress [Confining] (MPa)',
            **kwargs)

class Sigma1(Stress):
    def __init__(self, contour_limits=(-80, 0, 5), **kwargs):
        super(Sigma1, self).__init__(
            contour_limits=contour_limits,
            contour_colors=('grey', 'black'),
            component='minimum',
            name='Sigma1',
            title='Sigma1 (MPa)',
            **kwargs)

            
class JansenSectionView(SectionView):         
    def __init__(self, origin, normal, pit_offset=(0, 0, 0),
                 view_offset=170, vertical_location=-920,
                 camera_distance=25, **kwargs):
        super(JansenSectionView, self).__init__(
            origin=origin, normal=normal,
            pit_offset=pit_offset, view_offset=view_offset,
            vertical_location=vertical_location,
            camera_distance=camera_distance, **kwargs)


class Full(View):
    def __init__(self, **kwargs):
        super(Full, self).__init__(
            center=(107500, 111700, 5000),
            eye=(111300, 112700, 9000),
            roll=0,
            projection='perspective',
            name='Full-View',
            **kwargs)


class MiddleSection(JansenSectionView):
    def __init__(self, **kwargs):
        super(MiddleSection, self).__init__(
            normal=(0, 1, 0),
            origin=(164.25, 150.1, -900),
            name='Middle-Section',
            **kwargs)

class VerticalDisplacement(Displacement):
    def __init__(self, contour_max=0.1, contour_colors=('transparent', 'automatic'), **kwargs):
        contour_max = float(contour_max)
        contour_min = -0.3
        interval = 0.05
        component = 'Z'
        contour_limits = (contour_min, contour_max, interval)
        name = 'Displacement-z-{contour_max}'.format(contour_max=int(contour_max))

        super(VerticalDisplacement, self).__init__(
            name=name, contour_limits=contour_limits, component=component,
            contour_colors=contour_colors, **kwargs)
        self.layers.append(Geology(legend='off'))
        
class HorizontalDisplacement(Displacement):
    def __init__(self, contour_max=0.1, contour_colors=('transparent', 'automatic'), **kwargs):
        contour_max = float(contour_max)
        contour_min = -0.1
        component = 'X'
        interval = 0.02
        contour_limits = (contour_min, contour_max, interval)
        name = 'Displacement-Magnitude-{contour_max}'.format(contour_max=int(contour_max))

        super(HorizontalDisplacement, self).__init__(
            name=name, contour_limits=contour_limits, component=component,
            contour_colors=contour_colors, **kwargs)
        self.layers.append(Geology(legend='off'))
        
class StrengthStress(ContourPlot):
    def __init__(self, contour_limits=(0, 2, 0.5), title=None,
                 name=None, log_scale='off', component='von-mises',
                 **kwargs):

      
        name = 'Strength-Stress-Ratio'
       
        title = 'Strength-Stress-Ratio'

        plot_specifics = 'stress-strength-ratio reversed on above "lightgray" below "red" '
        
        super(StrengthStress, self).__init__(
            plot_specifics=plot_specifics,
            contour_limits=contour_limits,
            title=title,
            name=name,
            **kwargs)

class ZoneExtra(ContourPlot):
    def __init__(self, z_extra_slot, contour_limits=(1, 3, 0.25), title=None, name=None, **kwargs):

        if not title:
            title = 'Zone Extra {0}'.format(z_extra_slot)

        if not name:
            name = 'Zone-Extra-{0}'.format(z_extra_slot)

        plot_specifics = """extra type scalar index {z_extra} log off ...
                            source zone method constant ...
                            reversed on below 'lightgray' above 'blue' ...
                            """.format(
                                z_extra=z_extra_slot)

        super(ZoneExtra, self).__init__(plot_specifics=plot_specifics,
                                        title=title,
                                        name=name,
                                        contour_limits=contour_limits,
                                        **kwargs)