# -*- coding: utf-8 -*-

# importing modules from the standard python library
import sys
import os

# importing the itasca module to communicate with FLAC and set working
# directory to be the directory containing this file
import itasca as it
it.command('directory input')

# adding /lib to the python path - this is where the third party libraries and
# custom libraries are located
sys.path.append(os.path.join(os.getcwd(), 'lib'))

# import South Wall specific modules - this try/except block is neccessary to
# reload imported modules in an interactive environment
import main
reload(main)

import config
reload(config)

import temp
reload(temp)

def _01_zone_grouping(model, model_type='3D'):
    """Series of function calls to assign groupings to various aspects of the
    model"""

    # initialize new flac3d model and load the grid that was created in step 01
    model.new()
    model.generate_room_and_pillar_mesh()

    # refine the mesh where appropriate
    if model_type == '3D':
        model.refine_mesh_3d()
    elif model_type == '2D':
        model.cut_2d_section()
        model.refine_mesh_2d()
        
    #group the excavation stages
    model.group_rooms()
    if model_type == '2D':
        model.group_rooms_2d()
    

    # export the new grid file
    model.save_case_grid()


def _02_elastic_setup(model):
    """Setting up and solving for the initial elastic equilibrium"""

    # initialize new flac3d model and load the grid that was created in step 01
    model.new()
    model.load_case_grid()

    # set boundary conditions and gravity
    model.set_gravity()
    model.set_boundary_conditions()

    # assign material properties
    model.set_model_properties('geology', elastic=True)

    # assign initial stress conditions
    model.set_initial_stresses()
    model.save('Elastic-Setup')


def _03_elastic_solution(model):
    """Setting up and solving for the initial elastic equilibrium"""

    # initialize a run instance with the model
    run = main.Run()

    # excavate to 1999 pit shell and solve
    run.new_stage(model, 'Elastic-Solution', elastic=True)


def _04_plastic_setup(model):
    """Assigning plastic strength parameters to the different geomechanical
    units"""

    # assign material properties
    model.set_model_properties('geology')
    model.set_interface_properties(calibration)
    model.remove_interfaces()
    # group reaction force faces
    model.group_reaction_force_faces()

    # solve the model with updated plastic parameters
    model.save('Plastic-Setup')

def _05_excavate_mine(model, partial=False):
    """Running through each mining stage to find a static equilibrium"""

    # initializing the model run
    run = main.Run()

    if partial is False:
        # running each mining stage
        run.new_stage(model, 'Plastic-Solution', reset=True)
        run.initialize_for_creep()
        run.new_stage(model, 'ProductionRoom1-2D', reset=False)
        run.new_stage(model, 'ProductionRoom2-2D', reset=False)
        run.new_stage(model, 'ProductionRoom3-2D', reset=False)
        run.new_stage(model, 'ProductionRoom4-Pass1-2D', reset=False)
        run.new_stage(model, 'ProductionRoom4-Pass2-2D', reset=False)
        
    run.new_stage(model, 'ProductionRoom5-Pass1-Stage1-2D-15m', reset=False)
    run.resume_creeping(model, 'ProductionRoom5-Pass1-Stage1-2D-20m', config.MINING_STAGES['ProductionRoom5-Pass1-Stage1-2D-20m'].additional_creep_time, reset=False)
    run.resume_creeping(model, 'ProductionRoom5-Pass1-Stage1-2D-30m', config.MINING_STAGES['ProductionRoom5-Pass1-Stage1-2D-30m'].additional_creep_time, reset=False)
    run.resume_creeping(model, 'ProductionRoom5-Pass1-Stage1-2D-60m', config.MINING_STAGES['ProductionRoom5-Pass1-Stage1-2D-60m'].additional_creep_time, reset=False)
    run.resume_creeping(model, 'ProductionRoom5-Pass1-Stage1-2D', config.MINING_STAGES['ProductionRoom5-Pass1-Stage1-2D'].additional_creep_time, reset=False)

    run.new_stage(model, 'ProductionRoom5-Pass2-Stage1-2D-15m', reset=False)
    run.resume_creeping(model, 'ProductionRoom5-Pass2-Stage1-2D-20m', config.MINING_STAGES['ProductionRoom5-Pass2-Stage1-2D-20m'].additional_creep_time, reset=False)
    run.resume_creeping(model, 'ProductionRoom5-Pass2-Stage1-2D-30m', config.MINING_STAGES['ProductionRoom5-Pass2-Stage1-2D-30m'].additional_creep_time, reset=False)
    run.resume_creeping(model, 'ProductionRoom5-Pass2-Stage1-2D-60m', config.MINING_STAGES['ProductionRoom5-Pass2-Stage1-2D-60m'].additional_creep_time, reset=False)
    run.resume_creeping(model, 'ProductionRoom5-Pass2-Stage1-2D', config.MINING_STAGES['ProductionRoom5-Pass2-Stage1-2D'].additional_creep_time, reset=False)
#

if __name__ == '__main__':
    dry_run = False

   # initializing the model and model parameters
    model = main.Model(dry_run=dry_run)
    _01_zone_grouping(model, model_type='2D')
    _02_elastic_setup(model)
    _03_elastic_solution(model)
   # _04_plastic_setup(model)
# #_05_excavate_mine(model, partial=False)
#
    for calibration in [-919.3]:
        print calibration
        model = main.Model(dry_run=dry_run, calibration=calibration)
        run = main.Run()
        #restore_name ="K:/Projects/BHP/Jansen/1417600-24000_JansenProductionRoomSupport/05_Technical_Work/04_Analysis/FLAC3D_Model/02_Output_Files/01_Save_Files/2dmodel/040-variable-shadow-band/+2dmodel-040-variable-shadow-band_ProductionRoom4-Pass2-2D_CAL-" + str(calibration) + ".f3sav"
        #it.command('model restore "{restore_name}"'.format(restore_name=restore_name))
        _04_plastic_setup(model)
        run.set_clayseam_histories(calibration,'sb')
        run.set_clayseam_histories(-923.92,'402')
        _05_excavate_mine(model, partial=False)
        



