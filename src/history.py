
  
def general_creep_histories():
    cmd_str="""
        model history creep time-total
        model history timestep
        zone history unbalanced-force
     """
    it.command(cmd_str)

def set_underground_history(histories):

    
    for history in histories:
       it.command('zone history displacement-{direction} position {x},{y},{z}'.format(direction=history[0], x=history[1], y=history[2], z=history[3]))



def set_creep_model_histories(histories):
    general_creep_histories()
    set_underground_history(histories)
    
his_ref_x =	0 #left side of room
his_ref_y =	150 #middle of model
his_ref_z =	920 #top of room
elev_407 =	917.52
elev_402 =	923.95
    
x1=his_ref_x + -1
x2=his_ref_x + 0
x3=his_ref_x + 3.15
x4=his_ref_x + 6.3
x5=his_ref_x + 9.15
x6=his_ref_x + 12
x7=his_ref_x + 13

z1=his_ref_z + 2.5
z2=elev_407
z3=his_ref_z + 0
z4=his_ref_z + -0.95
z5=his_ref_z + -1.9
z6=his_ref_z + -2.85
z7=his_ref_z + -3.8
z8=elev_402
z9=his_ref_z + -6.3

DISP_HISTORIES = [("x",x1,his_ref_y,z1),
("x",x1,his_ref_y,z2),
("x",x1,his_ref_y,z3),
("x",x1,his_ref_y,z4),
("x",x1,his_ref_y,z5),
("x",x1,his_ref_y,z6),
("x",x1,his_ref_y,z7),
("x",x1,his_ref_y,z8),
("x",x1,his_ref_y,z9),
("x",x2,his_ref_y,z1),
("x",x2,his_ref_y,z2),
("x",x2,his_ref_y,z3),
("x",x2,his_ref_y,z4),
("x",x2,his_ref_y,z5),
("x",x2,his_ref_y,z6),
("x",x2,his_ref_y,z7),
("x",x2,his_ref_y,z8),
("x",x2,his_ref_y,z9),
("x",x6,his_ref_y,z1),
("x",x6,his_ref_y,z2),
("x",x6,his_ref_y,z3),
("x",x6,his_ref_y,z4),
("x",x6,his_ref_y,z5),
("x",x6,his_ref_y,z6),
("x",x6,his_ref_y,z7),
("x",x6,his_ref_y,z8),
("x",x6,his_ref_y,z9),
("x",x7,his_ref_y,z1),
("x",x7,his_ref_y,z2),
("x",x7,his_ref_y,z3),
("x",x7,his_ref_y,z4),
("x",x7,his_ref_y,z5),
("x",x7,his_ref_y,z6),
("x",x7,his_ref_y,z7),
("x",x7,his_ref_y,z8),
("x",x7,his_ref_y,z9),
("z",x1,his_ref_y,z1),
("z",x1,his_ref_y,z2),
("z",x1,his_ref_y,z3),
("z",x1,his_ref_y,z7),
("z",x1,his_ref_y,z8),
("z",x1,his_ref_y,z9),
("z",x2,his_ref_y,z1),
("z",x2,his_ref_y,z2),
("z",x2,his_ref_y,z3),
("z",x2,his_ref_y,z7),
("z",x2,his_ref_y,z8),
("z",x2,his_ref_y,z9),
("z",x3,his_ref_y,z1),
("z",x3,his_ref_y,z2),
("z",x3,his_ref_y,z3),
("z",x3,his_ref_y,z7),
("z",x3,his_ref_y,z8),
("z",x3,his_ref_y,z9),
("z",x4,his_ref_y,z1),
("z",x4,his_ref_y,z2),
("z",x4,his_ref_y,z3),
("z",x4,his_ref_y,z7),
("z",x4,his_ref_y,z8),
("z",x4,his_ref_y,z9),
("z",x5,his_ref_y,z1),
("z",x5,his_ref_y,z2),
("z",x5,his_ref_y,z3),
("z",x5,his_ref_y,z7),
("z",x5,his_ref_y,z8),
("z",x5,his_ref_y,z9),
("z",x6,his_ref_y,z1),
("z",x6,his_ref_y,z2),
("z",x6,his_ref_y,z3),
("z",x6,his_ref_y,z7),
("z",x6,his_ref_y,z8),
("z",x6,his_ref_y,z9),
("z",x7,his_ref_y,z1),
("z",x7,his_ref_y,z2),
("z",x7,his_ref_y,z3),
("z",x7,his_ref_y,z7),
("z",x7,his_ref_y,z8),
("z",x7,his_ref_y,z9)
]

set_creep_model_histories(DISP_HISTORIES)    